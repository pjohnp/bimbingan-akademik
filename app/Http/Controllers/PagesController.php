<?php

namespace App\Http\Controllers;

use App\Appointment;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    //
    public function landing()
    {
        $today = CarbonImmutable::now(new \DateTimeZone('Asia/Jakarta'));
        $all_apt = Appointment::all();

        foreach ($all_apt as $index=>$past_apt)
        {
            if ($today->lte($past_apt->date_time) == false)
            {
                $done = Appointment::find($past_apt->id);
                $done->done = 1;
                $done->save();
            }
        }

        return view('welcome');
    }
}
