<?php

namespace App\Http\Controllers;

use App\Absensi;
use App\Appointment;
use App\Schedule;
use App\User;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $today = CarbonImmutable::now(new \DateTimeZone('Asia/Jakarta'));
        $appointment = Appointment::where('dosen_id','=',Auth::user()->dosen_id)->where('done','=',0)->get();
        $appointment_done = Appointment::where('dosen_id','=',Auth::user()->dosen_id)->where('done','=',1)->get();
        $details = Absensi::where('dosen_id','=',Auth::user()->dosen_id)->get();
        $schedule = Schedule::where('dosen_id','=',Auth::user()->dosen_id)->get();

//        $name = array();
//        foreach ($pasts as $past)
//        {
//            array_push($name,$past->name);
//        }
//        dd($appointment_done[0]->updated_at->isoFormat('dddd D'));

        return view('student',compact('appointment','appointment_done','today','details','schedule'));
    }

    public function create(Request $request)
    {
//        $this->validate($request,
//            [
//                'name'=>'required|string|max:255',
//                'email' => 'required|string|email|max:255|unique:dosen',
//                'password'=>'required|string|password|max:255'
//            ]);
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->dosen_id = $request->dosen_id;
        $user->prodi_id = $request->prodi_id;
        $user->save();
//        dd($request);
        return redirect('/admin');
    }

    public function removeAppointment(Request $request)
    {

    }
}
