<?php

namespace App\Http\Controllers;

use App\Prodi;
use Illuminate\Http\Request;

class ProdiController extends Controller
{
    //
    public function create(Request $request)
    {
        $prodi = new Prodi();
        $prodi->name = $request->name;
        $prodi->save();
        return redirect('/admin');
    }
}
