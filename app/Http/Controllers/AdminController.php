<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Prodi;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $prodi = Prodi::all();
        $dosen = User::where('role','=','dosen')->get();
        $appointments = Appointment::all();
//        dd($dosen[0]->Prodi->name);
//        dd($dosen);
        return view('admin',compact('prodi','dosen','appointments'));
    }

    public function updateAbsensi(Request $request)
    {
        return "absensi admin";
    }
}
