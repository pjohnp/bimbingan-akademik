<?php

namespace App\Http\Controllers;

use App\Absensi;
use App\Appointment;
use App\Helpers\Globals;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AppointmentController extends Controller
{
    //

    public function create(Request $request)
    {
        $students = User::where('dosen_id','=',Auth::user()->id)->get();
        if (Auth::user()->role == 'dosen')
        {
            $new = new Appointment();
            $new->name = $request->name;
            $new->date_time = $request->date ." ". $request->time;
            $new->dosen_id = $request->dosen_id;
            $new->purposed_by = $request->purposed_by;
            $new->approved = 1;
            $new->done = 0;

            //menginformasikan semua student melalui email
            $new->save();
            foreach ($students as $student)
            {
                $student = User::find($student->id);
                $student->appointment_id = $new->id;
                $student->save();
            }
        }
        else
        {
            $new = new Appointment();
            $new->name = $request->name;
            $new->date_time = $request->date . $request->time;
            $new->dosen_id = $request->dosen_id;
            $new->purposed_by = $request->purposed_by;
            $new->approved = 0;
            $new->done = 0;
            $new->save();
        }
        return redirect(Globals::main_url());
    }

    public function delete(Request $request,$id)
    {
        $appointment = Appointment::find($id);
        $appointment->delete($appointment);
        return redirect(Globals::main_url());
    }

    public function accept(Request $request,$id)
    {
        $appointment = Appointment::find($id);
        $appointment->approved = 1;
        $appointment->done = 0;
        $appointment->save();
        return redirect('/dosen');
    }

    public function reject(Request $request,$id)
    {
        $appointment = Appointment::find($id);
        $students = User::where('dosen_id','=',$appointment->dosen_id)->get();
        foreach ($students as $index=>$student)
        {
//            dd($student->id);
            $absensi = new Absensi();
            $absensi->appointment_id = $appointment->id;
            $absensi->student_id = $student->id;
            $absensi->student_name = User::find($student->id)->name;
            $absensi->present = 0;
            $absensi->dosen_id = Auth::user()->id;
            $absensi->summary = 'Ditolak';
            $absensi->save();
        }
//        dd($students);
        $appointment->approved = 0;
        $appointment->done = 1;
        $appointment->save();
        return redirect(Globals::main_url());
    }
}
