<?php

namespace App\Http\Controllers;

use App\Absensi;
use App\Appointment;
use App\Schedule;
use App\User;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class DosenController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $appointments = Appointment::where('dosen_id','=',Auth::user()->id)->where('done','=',0)->get();
        $appointments_done = Appointment::where('dosen_id','=',Auth::user()->id)->where('done','=',1)->get();
        $schedule = Schedule::where('dosen_id','=',Auth::user()->id)->get();
        $students = User::where('dosen_id','=',Auth::user()->id)->get();
        $pending = Appointment::where('dosen_id','=',Auth::user()->id)->where('approved','=',0)->get();

        $purposed_name = array();
        foreach ($appointments as $sender)
        {
            $name = User::find($sender->purposed_by)->name;
            array_push($purposed_name,$name);
        }

//        dd($purposed_name);
        return view('dosen',compact('students','schedule','appointments','pending','appointments_done','purposed_name'));
    }

    public function create(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role = 'dosen';
        $user->prodi_id = $request->prodi_id;
        $user->save();
//        dd($user);
        return redirect('/admin');
    }

    public function addSchedule(Request $request)
    {
        $schedule = new Schedule();
        $schedule->name = $request->name;
        $schedule->time = $request->time;
        $schedule->dosen_id = Auth::user()->id;
        $schedule->save();
        return redirect('/dosen');
    }

    public function updateAbsensi(Request $request)
    {
        //passing the array of the student`s id
        $students = $request->id;

        //make a new data for each student
        foreach ($students as $index=>$student)
        {
            $absensi = new Absensi();
            $absensi->appointment_id = $request->appointment_id;
            $absensi->student_id = $student;
            $absensi->student_name = User::find($student)->name;
            $absensi->present = $request->attedance[$index];
            $absensi->dosen_id = $request->dosen_id;
            $absensi->summary = 'Ditolak';
            $absensi->save();
        }

        //update appointment to done
        $appointment = Appointment::find($request->appointment_id);
        $appointment->done = 1;
        $appointment->save();
        return redirect('/dosen');
//            dd($request->all());
    }
}
