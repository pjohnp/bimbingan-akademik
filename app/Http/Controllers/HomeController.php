<?php

namespace App\Http\Controllers;

use App\Dosen;
use App\Prodi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $prodi = Prodi::all();
        $dosen = Dosen::all();
//        dd(Auth::user());
        return view('home',compact('prodi','dosen'));
    }
}
