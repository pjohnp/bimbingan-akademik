<?php

namespace App\Helpers;

use App\Absensi;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Globals
{
    public static function main_url()
    {
        if (Auth::check()) {
            switch (Auth::user()->role) {
                case "admin":
                    return '/admin';
                    break;
                case  "dosen":
                    return '/dosen';
                    break;
                default :
                    return '/home';
            }
        } else {
            switch (Auth::user()->role) {
                case "admin":
                    return '/admin';
                    break;
                case  "dosen":
                    return '/dosen';
                    break;
                default :
                    return '/home';
            }
        }
    }

    public static function DateTimeFormat($date)
    {
        return Carbon::parse($date)->format('h:i F jS\\, Y');
    }

    public static function Summary($id)
    {
        return Absensi::where('appointment_id','=',$id)->first()->summary;
    }
}


