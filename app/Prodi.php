<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    //
    protected $table = 'prodi';
    protected $fillable = ['name'];

    public function User()
    {
        return $this->hasMany('App\User');
    }
}
