<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    //
//    protected $table = 'appointment';

    protected $fillable = ['name','date_time','purposed_by','dosen_id','approved','done'];

    public function User()
    {
        return $this->belongsTo('App\User');
    }

    public function Absensi()
    {
        return $this->hasMany('App\Absensi');
    }
}
