<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    //
    protected $table = 'absensi';

    protected $fillable = ['student_name','appointment_id','present','dosen_id'];

    public function Appointment()
    {
        return $this->belongsTo('App\Appointment');
    }
}
