<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    //
    protected $fillable = ['time','name','dosens_id'];

    protected $table = 'schedule';

    public function User()
    {
        return $this->belongsTo('App\User');
    }
}
