<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routesh
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@landing');
//Route::post('/userlogin','UserLoginController@login');

Auth::routes();

Route::middleware('auth')->group(function ()
{
    Route::post('/addappointment','AppointmentController@create');
    Route::post('/updateappointment','AppointmentController@update');
});

Route::middleware('admin')->group(function ()
{
    Route::get('/admin', 'AdminController@index');
    Route::post('/addprodi', 'ProdiController@create');
    Route::post('/addstudent','StudentController@create');
    Route::post('/adddosen','DosenController@create');
//    Route::get('/update_absensi','AdminController@updateAbsensi');
});

Route::middleware('dosen')->group( function ()
{
    Route::get('/dosen', 'DosenController@index');
    Route::post('/addschedule', 'DosenController@addSchedule');
    Route::post('/{id}/rejectappointment','AppointmentController@reject');
    Route::post('/{id}/accappointment','AppointmentController@accept');
    Route::post('/{id}/delappointment','AppointmentController@delete');
    Route::post('/update_absensi','DosenController@updateAbsensi');
});

Route::middleware('auth')->group( function ()
{
    Route::get('/home', 'StudentController@index');
});

