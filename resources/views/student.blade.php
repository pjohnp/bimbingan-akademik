@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 row">

            <div class="col-4">
                <!-- Nav tabs -->
                <ul class="nav md-pills pills-#66fcf1 flex-column" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#panel555" role="tab">
                            <i class="fas fa-user pr-2"></i>Current</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#panel666" role="tab">
                            <i class="fas fa-calendar-check pr-2"></i>Done</a>
                    </li>
                </ul>
                <!-- Nav tabs -->
            </div>

            <div class="col-8">
                <!-- Tab panels -->
                <div class="tab-content" style="padding: 0">

                    <!-- Panel 1 -->
                    <div class="tab-pane fade in show active" id="panel555" role="tabpanel">

                        <!-- Nav tabs -->
                        <div class="row">
                            <div class="col-md-3">
                                <ul class="nav md-pills pills-#66fcf1 flex-column" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#panel21" role="tab">
                                            <i class="fas fa-calendar-day"></i> Upcoming
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#panel23" role="tab">
                                            <i class="fas fa-file-alt"></i> Make Appointment
                                        </a>
                                    </li>
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" data-toggle="tab" href="#panel22" role="tab">Teacher--}}
{{--                                            <i class="fas fa-chalkboard-teacher"></i>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" data-toggle="tab" href="#panel23" role="tab">Student--}}
{{--                                            <i class="fas fa-user-graduate"></i>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
                                </ul>
                            </div>
                            <div class="col-md-9">
                                <!-- Tab panels -->
                                <div class="tab-content vertical pt-3">
                                    <!-- Panel 1 -->
                                    <div class="tab-pane fade in show active" id="panel21" role="tabpanel">
                                        <h5>
                                            @if(count($appointment) != 0)
                                                @if ($today->lte($appointment->first()->date_time))
                                                    @foreach($appointment as $jadwal)
                                                        <div class="col">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="card-title text-muted">{{$jadwal->name}}</div>
                                                                    <div class="card-text">{{$jadwal->date_time}}</div>
                                                                </div>
                                                                @if($jadwal->approved == 0)
                                                                    <div class="rounded-bottom text-center pt-3">
                                                                        <ul class="list-unstyled">
                                                                            <li>
                                                                                <form action="/{{$jadwal->id}}/delappointment" method="post">
                                                                                    <button class="btn btn-rounded border-danger" type="submit" onclick="return confirm('Are you sure want to delete this appointment ?')">Delete <i class="fas fa-trash"></i></button>
                                                                                </form>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @else
                                                    <p>You just missed the appointment</p>
                                                @endif
                                            @else
                                                <p>There`s no appointment</p>
                                            @endif
                                        </h5>
                                    </div>
                                    <!-- Panel 1 -->
                                    <!-- Panel 3 -->
                                    <div class="tab-pane fade" id="panel23" role="tabpanel">
                                        <h5>
                                            <form action="/addappointment" method="post">
                                                @csrf
                                                <div class="form-group row">
                                                    <div class="col-md-3">Name</div>
                                                    <div class="col-md-9">
                                                        <input type="text" name="name" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-3">Date</div>
                                                    <div class="col-md-9">
                                                        <input type="date" name="date" class="form-control" required>
                                                    </div>
                                                </div>
                                                @if(count($schedule) != 0)
                                                    <div class="form-group row">
                                                        <div class="col-md-3">Time</div>
                                                        <div class="col-md-9">
                                                            <select class="browser-default custom-select" name="time" required>
                                                                <option value="" disabled selected>Pilih Waktu</option>
                                                                @foreach($schedule as $pilihan)
                                                                    <option
                                                                        value="{{$pilihan->time}}">{{$pilihan->time}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                @else
                                                @endif
                                                <input type="hidden" name="dosen_id" value="{{auth()->user()->dosen_id}}">
                                                <input type="hidden" name="purposed_by" value="{{auth()->user()->id}}">
                                                <div class="text-center">
                                                    <button type="submit" class="btn btn-outline-default btn-rounded waves-effect" onclick="return confirm('Are you sure to submit this appointment ?')">Submit</button>
                                                </div>
                                            </form>
                                        </h5>
                                    </div>
                                    <!-- Panel 3 -->
                                </div>
                            </div>
                        </div>
                        <!-- Nav tabs -->

                    </div>
                    <!-- Panel 1 -->

                    <!-- Panel 2 -->
                    <div class="tab-pane fade" id="panel666" role="tabpanel">

                        @if(count($appointment_done) != 0)
                            @foreach($appointment_done as $index=>$done)
                                <div class="col pb-2">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="card-title">
                                                <div class="row">
                                                    <div class="col-9">
                                                        <span class="text-muted">{{$done->name}}</span>
                                                    </div>
                                                    <div class="col-3 text-right">
                                                        <span class="text-muted" style="font-size: 12px">
                                                            {{$done->updated_at->isoFormat('dddd D')}}
                                                            <br>
                                                            @if($details[$index]->present == 1)
                                                                hadir
                                                            @else
                                                                tidak hadir
                                                            @endif
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-text">{{$details[$index]->summary}}</div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <p>Theres none of it done, yet</p>
                        @endif

                    </div>
                    <!-- Panel 2 -->

                </div>
                <!-- Tab panels -->
            </div>
        </div>
    </div>
</div>
@endsection
