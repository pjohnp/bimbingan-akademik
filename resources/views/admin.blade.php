@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 row">

                <div class="col-4">
                    <!-- Nav tabs -->
                    <ul class="nav md-pills pills-#66fcf1 flex-column" role="tablist"> {{--UPDATE--}}
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#panel555" role="tab"> {{--UPDATE--}}
                                <i class="fas fa-plus-circle pr-2"></i>Create</a>{{--UPDATE--}}
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#panel666" role="tab">
                                <i class="fas fa-calendar-check"></i> Done!</a> {{--UPDATE--}}
                        </li>
                    </ul>
                    <!-- Nav tabs -->
                </div>

                <div class="col-8">
                    <!-- Tab panels -->
                    <div class="tab-content" style="padding: 0">

                        <!-- Panel 1 -->
                        <div class="tab-pane fade in show active" id="panel555" role="tabpanel">

                            <!-- Nav tabs -->
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="nav md-pills pills-#66fcf1 flex-column" role="tablist"> {{--UPDATE--}}
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#panel21" role="tab">Prodi
                                                <i class="fas fa-book-reader"></i> {{--UPDATE--}}
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#panel22" role="tab">Dosen
                                                <i class="fas fa-chalkboard-teacher"></i> {{--UPDATE--}}
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#panel23" role="tab">Mahasiswa
                                                <i class="fas fa-user-graduate"></i> {{--UPDATE--}}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-9">
                                    <!-- Tab panels -->
                                    <div class="tab-content vertical pt-3">
                                        <!-- Panel 1 -->
                                        <div class="tab-pane fade in show active" id="panel21" role="tabpanel">
                                            <h5>
                                                <form action="/addprodi" method="post">
                                                    @csrf
                                                    <div class="form-group row">
                                                        <div class="col-md-3">Nama</div>
                                                        <div class="col-md-9">
                                                            <input type="text" name="name" class="form-control"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    <div class="text-center">
                                                        <button type="submit" class="btn btn-primary"
                                                                onclick="return confirm('Are you sure the form is correct ?')">
                                                            Submit
                                                        </button>
                                                    </div>
                                                </form>
                                            </h5>
                                        </div>
                                        <!-- Panel 1 -->
                                        <!-- Panel 2 -->
                                        <div class="tab-pane fade" id="panel22" role="tabpanel">
                                            <h5>
                                                <form action="/adddosen" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="form-group row">
                                                        <div class="col-md-3">Name</div>
                                                        <div class="col-md-9">
                                                            <input type="text" name="name" class="form-control"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-3">Prodi</div>
                                                        <div class="col-md-9">
                                                            <select class="browser-default custom-select"
                                                                    name="prodi_id" required>
                                                                @if(count($prodi) > 0)
                                                                    <option value="" disabled selected>Pilih Prodi
                                                                    </option>
                                                                    @foreach($prodi as $pilihan)
                                                                        <option
                                                                            value="{{$pilihan->id}}">{{$pilihan->name}}</option>
                                                                    @endforeach
                                                                @else
                                                                    <option value="" disabled selected>Buat Prodi
                                                                        terlebih dahulu
                                                                    </option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-3">E-mail</div>
                                                        <div class="col-md-9">
                                                            <input type="email" name="email" class="form-control"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-3">Password</div>
                                                        <div class="col-md-9">
                                                            <input type="password" name="password" class="form-control"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    <div class="text-center">
                                                        <button type="submit" class="btn btn-primary"
                                                                onclick="return confirm('Are you sure the form is correct ?')">
                                                            Submit
                                                        </button>
                                                    </div>
                                                </form>
                                            </h5>
                                        </div>
                                        <!-- Panel 2 -->
                                        <!-- Panel 3 -->
                                        <div class="tab-pane fade" id="panel23" role="tabpanel">
                                            <h5 class="my-2 h5">
                                                <form action="/addstudent" method="post">
                                                    @csrf
                                                    <div class="form-group row">
                                                        <div class="col-md-3">Nama</div>
                                                        <div class="col-md-9">
                                                            <input type="text" name="name" class="form-control"
                                                                   required>
                                                            <input type="hidden" name="role" value="2">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-3">Email</div>
                                                        <div class="col-md-9">
                                                            <input type="email" name="email" class="form-control"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-3">Prodi</div>
                                                        <div class="col-md-9">
                                                            <select class="browser-default custom-select"
                                                                    name="prodi_id" required>
                                                                @if(count($prodi) > 0)
                                                                    <option value="" disabled selected>Pilih Prodi
                                                                    </option>
                                                                    @foreach($prodi as $pilihan)
                                                                        <option
                                                                            value="{{$pilihan->id}}">{{$pilihan->name}}</option>
                                                                    @endforeach
                                                                @else
                                                                    <option value="" disabled selected>Buat Prodi
                                                                        terlebih dahulu
                                                                    </option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-3">Dosen</div>
                                                        <div class="col-md-9">
                                                            <select class="browser-default custom-select"
                                                                    name="dosen_id" required>
                                                                @if(count($dosen) > 0)
                                                                    <option value="" disabled selected>Pilih Dosen
                                                                    </option>
                                                                    @foreach($dosen as $pilihan)
                                                                        <option
                                                                            value="{{$pilihan->id}}">{{$pilihan->name}}(<strong>{{$pilihan->Prodi->name}}</strong>)
                                                                        </option>
                                                                    @endforeach
                                                                @else
                                                                    <option value="" disabled selected>Buat Akun Dosen
                                                                        terlebih dahulu
                                                                    </option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-3">Password</div>
                                                        <div class="col-md-9">
                                                            <input type="password" name="password" class="form-control"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    <div class="text-center">
                                                        <button type="submit" class="btn btn-primary"
                                                                onclick="return confirm('Are you sure the form is correct ?')">
                                                            Submit
                                                        </button>
                                                    </div>
                                                </form>
                                            </h5>
                                        </div>
                                        <!-- Panel 3 -->
                                    </div>
                                </div>
                            </div>
                            <!-- Nav tabs -->

                        </div>
                        <!-- Panel 1 -->

                        <!-- Panel 2 -->
                        <div class="tab-pane fade" id="panel666" role="tabpanel">
                            <div class="row"></div>
                            @foreach($appointments as $appointment)
                                <div class="col pb-2">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="card-title text-muted">{{$appointment->name}}</div>
                                            <div class="card-text">
                                                <div class="row justify-content-center">
                                                    <div class="col">
                                                        <span class="text-muted">{{\App\User::find($appointment->dosen_id)->name}}</span>
                                                        @if($appointment->done)
                                                            <p>{{$appointment->id}}</p>
                                                        @endif
                                                    </div>
                                                    <div
                                                        class="col-2 pr-1 text-right">{{\App\Helpers\Globals::DateTimeFormat($appointment->date_time)}}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                        <!-- Panel 2 -->

                    </div>
                    <!-- Tab panels -->
                </div>
            </div>
        </div>
    </div>
@endsection
