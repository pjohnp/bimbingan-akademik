@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 row">

                <div class="col-4">
                    <!-- Nav tabs -->
                    <ul class="nav md-pills pills-#66fcf1 flex-column" role="tablist">
                        <li class="nav-item">
                            {{--UPDATE--}}<a class="nav-link active" data-toggle="tab" href="#panel555" role="tab">
                                {{--UPDATE--}} <i class="fas fa-plus-circle pr-2"></i>Create</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#panel666" role="tab">
                                {{--UPDATE--}}<i class="fas fa-calendar-check"></i> Done!</a>
                        </li>
                    </ul>
                    <!-- Nav tabs -->
                </div>

                <div class="col-8">
                    <!-- Tab panels -->
                    <div class="tab-content" style="padding: 0">

                        <!-- Panel 1 -->
                        <div class="tab-pane fade in show active" id="panel555" role="tabpanel">

                            <!-- Nav tabs -->
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="nav md-pills pills-#66fcf1 flex-column" role="tablist">
                                        <li class="nav-item">
                                            {{--UPDATE--}} <a class="nav-link active" data-toggle="tab" href="#panel21" role="tab">Current
                                                Appointment(s)
                                                {{--UPDATE--}} <i class="far fa-list-alt"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#panel22" role="tab">Create
                                                Appointment
                                                {{--UPDATE--}} <i class="far fa-calendar-alt"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#panel23" role="tab">Create
                                                Schedule
                                                {{--UPDATE--}} <i class="far fa-calendar-alt"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#panel24" role="tab">Attedance
                                                List
                                                {{--UPDATE--}} <i class="fas fa-clipboard-list"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-9">
                                    <!-- Tab panels -->
                                    <div class="tab-content vertical pt-3">
                                        <!-- Panel Appointment -->
                                        <div class="tab-pane fade in show active" id="panel21" role="tabpanel">
                                            <div class="row justify-content-center">
                                                @if(count($appointments) != 0)
                                                    @foreach($appointments as $index=>$appointment)
                                                        <div class="col">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="card-title text-muted">{{$appointment->name}}</div>
                                                                    <div class="card-text">
                                                                        <p>{{$appointment->date_time}}</p>
                                                                        @if(count($purposed_name) != 0)
                                                                            <p><span
                                                                                    class="text-muted">Purposed by : </span>{{$purposed_name[$index]}}
                                                                            </p>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                @if($appointment->approved == null)
                                                                    <div class="rounded-bottom text-center pt-3">
                                                                        <div class="row justify-content-center">
                                                                            <div class="col p-0">
                                                                                <form
                                                                                    action="/{{$appointment->id}}/rejectappointment"
                                                                                    method="post">
                                                                                    @csrf
                                                                                    <button
                                                                                        class="btn btn-rounded border-danger"
                                                                                        type="submit">
                                                                                        <i class="fas fa-window-close"></i>
                                                                                        Reject
                                                                                    </button>
                                                                                </form>
                                                                            </div>
                                                                            <div class="col p-0 ">
                                                                                <form
                                                                                    action="/{{$appointment->id}}/accappointment"
                                                                                    method="post">
                                                                                    @csrf
                                                                                    <button
                                                                                        class="btn btn-rounded bg-success"
                                                                                        type="submit">
                                                                                        <i class="fas fa-check"></i>
                                                                                        Accept
                                                                                    </button>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                                <div class="card-footer text-center">
                                                                    <form action="/{{$appointment->id}}/delappointment"
                                                                          method="post">
                                                                        @csrf
                                                                        <button type="submit"
                                                                                class="btn btn-md btn-rounded btn-outline-danger"
                                                                                onclick="return confirm('Are you sure want to Cancel this appointment ?')">
                                                                            <i class="fas fa-trash"></i> Cancel
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @else
                                                @endif
                                                <hr>

                                            </div>
                                        </div>
                                        <!-- Panel Appointment -->
                                        <!-- Panel Add Appointment -->
                                        <div class="tab-pane fade" id="panel22" role="tabpanel">
                                            <h5>
                                                <form action="/addappointment" method="post">
                                                    @csrf
                                                    <div class="form-group row">
                                                        <div class="col-md-3">Name</div>
                                                        <div class="col-md-9">
                                                            <input type="text" name="name" class="form-control"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-3">Date</div>
                                                        <div class="col-md-9">
                                                            <input type="date" name="date" class="form-control"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    @if(count($schedule) != 0)
                                                        <div class="form-group row">
                                                            <div class="col-md-3">Time</div>
                                                            <div class="col-md-9">
                                                                <select class="browser-default custom-select"
                                                                        name="time">
                                                                    <option value="" disabled selected>Pilih Waktu
                                                                    </option>
                                                                    @foreach($schedule as $pilihan)
                                                                        <option
                                                                            value="{{$pilihan->time}}">{{$pilihan->time}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    @else
                                                    @endif
                                                    <input type="hidden" name="dosen_id" value="{{auth()->user()->id}}">
                                                    <input type="hidden" name="purposed_by"
                                                           value="{{auth()->user()->id}}">
                                                    <div class="text-center">
                                                        <button type="submit" class="btn btn"
                                                                onclick="return confirm('Are you sure to submit this appointment ?')">
                                                            Submit
                                                        </button>
                                                    </div>
                                                </form>
                                            </h5>
                                        </div>
                                        <!-- Panel Add Appointment -->
                                        <!-- Panel Add Schedule -->
                                        <div class="tab-pane fade" id="panel23" role="tabpanel">
                                            <h5>
                                                <form action="/addschedule" method="post">
                                                    @csrf
                                                    <div class="form-group row">
                                                        <div class="col-md-3">Name</div>
                                                        <div class="col-md-9">
                                                            <input type="text" name="name" class="form-control"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-3">Time</div>
                                                        <div class="col-md-9">
                                                            <input type="time" name="time" class="form-control"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="dosen_id" value="{{auth()->user()->id}}">
                                                    <div class="text-center">
                                                        <button type="submit" class="btn btn-outline-default btn-rounded waves-effect"
                                                                onclick="return confirm('Are you available at this hour ?')">
                                                            Submit
                                                        </button>
                                                    </div>
                                                </form>
                                            </h5>
                                            <div class="row justify-content-center">
                                                @if(count($schedule) != 0)
                                                    @foreach($schedule as $jadwal)
                                                        <div class="col">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="card-title">{{$jadwal->name}}</div>
                                                                    <div class="card-text">{{$jadwal->time}}</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @else
                                                @endif
                                            </div>
                                        </div>
                                        <!-- Panel Add Schedule -->
                                        <!-- Panel Attedance List -->
                                        <div class="tab-pane fade" id="panel24" role="tabpanel">
                                            <h5>
                                                <div class="accordion md-accordion" id="accordionEx" role="tablist"
                                                     aria-multiselectable="true">
                                                    <!-- Appointment card -->
                                                    @foreach($appointments as $appointment)
                                                        @if($appointment->approved)
                                                            <div class="card">
                                                                <form action="/update_absensi" method="post">
                                                                    @csrf
                                                                    <input type="hidden" name="appointment_id"
                                                                           value="{{$appointment->id}}">
                                                                    <!-- Card header -->
                                                                    <div class="card-header" role="tab"
                                                                         id="headingOne1">
                                                                        <a data-toggle="collapse"
                                                                           data-parent="#accordionEx"
                                                                           href="#Appointment_{{$appointment->id}}"
                                                                           aria-expanded="true"
                                                                           aria-controls="Appointment_{{$appointment->id}}">
                                                                            <h5 class="mb-0">
                                                                                {{$appointment->name}}
                                                                                <i class="fas fa-angle-down rotate-icon"></i>
                                                                            </h5>
                                                                        </a>
                                                                    </div>

                                                                    <!-- Card body -->
                                                                    <div id="Appointment_{{$appointment->id}}"
                                                                         class="collapse show row justify-content-center"
                                                                         role="tabpanel" aria-labelledby="headingOne1"
                                                                         data-parent="#accordionEx">
                                                                        <div class="col-12 card-body">
                                                                            <textarea class="md-textarea form-control"
                                                                                      rows="2" name="summary"
                                                                                      placeholder="Summary"
                                                                                      required></textarea>
                                                                            @foreach($students as $index=>$student)
                                                                                <div class="col-12 form-group row">
                                                                                    <div
                                                                                        class="col-md-3">{{$student->name}}</div>
                                                                                    <div class="col-md-9">
                                                                                        <div class="form-check">
                                                                                            <input type="hidden"
                                                                                                   name="id[]"
                                                                                                   value="{{$student->id}}">
                                                                                            <input type="hidden"
                                                                                                   name="attedance[{{$index}}]"
                                                                                                   value="0">
                                                                                            <input type="checkbox"
                                                                                                   class="form-check-input"
                                                                                                   id="attedance_{{$student->id}}"
                                                                                                   name="attedance[{{$index}}]"
                                                                                                   value="1">
                                                                                            <label
                                                                                                for="attedance_{{$student->id}}">Present</label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                        </div>
                                                                        <input type="hidden" name="dosen_id"
                                                                               value="{{auth()->user()->id}}">
                                                                        <button class="col-3 btn btn-rounded"
                                                                                type="submit"
                                                                                onclick="return confirm('Are you sure want to submit this attedance list ?')">
                                                                            Submit
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                    @else
                                                    @endif
                                                @endforeach
                                                <!-- Appointment card -->
                                                </div>
                                            </h5>
                                        </div>
                                        <!-- Panel Attedance List -->
                                    </div>
                                </div>
                            </div>
                            <!-- Nav tabs -->

                        </div>
                        <!-- Panel 1 -->

                        <!-- Panel 2 -->
                        <div class="tab-pane fade" id="panel666" role="tabpanel">

                            <div class="row">
                                @foreach($appointments_done as $done)
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="card-title m-0">
                                                    <span class="text-muted">{{$done->name}}</span>
                                                </div>
                                                <div class="card-text pt-2">
                                                    <div class="row">
                                                        <div
                                                            class="col text-muted">{{\App\Helpers\Globals::Summary($done->id)}}</div>
                                                        <div
                                                            class="col-2 text-right">{{$done->updated_at->format('h:i F jS\\, Y')}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                        <!-- Panel 2 -->

                    </div>
                    <!-- Tab panels -->
                </div>
            </div>
        </div>
    </div>
@endsection
