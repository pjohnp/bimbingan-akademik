<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAppointments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('appointments',function (Blueprint $table)
        {
            $table->boolean('approved')->nullable()->default(0);
            $table->bigInteger('purposed_by')->unsigned();
            $table->foreign('purposed_by')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('dosen_id')->unsigned();
            $table->foreign('dosen_id')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('done')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
